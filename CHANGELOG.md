# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased](unreleased)

## [0.1.1](2015-09-26)
- Fixed: Return values from a few internal packages
- Fixed: Mysterious segfault on attempting Pkg.build(...) from within this package.
- Fixed: Compatibility with runtests.jl and build.jl files for ease of use
- Fixed: Julia v0.4 compatibility

## [0.1.0](2015-04-21)
- Added: Working implementation of basic resolution for file and unregistered package dependencies.
- Added: Examples for file dependencies
