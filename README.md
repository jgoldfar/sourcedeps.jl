# SourceDeps

[![Build Status](https://travis-ci.org/jgoldfar/SourceDeps.jl.svg?branch=master)](https://travis-ci.org/jgoldfar/SourceDeps.jl)

SourceDeps.jl is a lightweight package for installing and managing source-only dependencies and unregistered packages from external sources.

In this way, it is complimentary to BinDeps, but has out-of-the-box support for `make clean` and source updating. Right now (in a very un-Julian way) it only supports source-only Mercurial repositories, but Git repos are in the plans.

Current: v0.1.1

## Installation ##
Install initially by running

```
Pkg.clone("git@bitbucket.org:jgoldfar/sourcedeps.jl.git", "SourceDeps")
```

After that, to update to the latest version, just run
```
Pkg.checkout("SourceDeps")
```

## Usage ##

See the files under `deps` for example calls into SourceDeps.
