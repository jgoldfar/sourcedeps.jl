module testdeps
include(joinpath(dirname(@__FILE__), "..", "src", "SourceDeps.jl"))
using .SourceDeps

const exfn = FileDependency(
  "exfn",
  "ssh://hg@bitbucket.org/jgoldfar/isp-exfn",
  [("src", joinpath("test", "exfn")),],
  version = v"1.0"
  )
const pkgdeps = (exfn, )
srcroot, pkgroot = SourceDeps.@setup

const main = SourceDeps.@mainfn
main(["install"])
# SourceDeps.@checkout exfn
# SourceDeps.@clean exfn
# SourceDeps.@clean_dep exfn

end
