module testdeps
include(joinpath(dirname(@__FILE__), "..", "src", "SourceDeps.jl"))
using .SourceDeps

srcroot, pkgroot = SourceDeps.@setup

xunittest = FileDependency(
  "xunittest",
  "ssh://hg@bitbucket.org/jgoldfar/xunittest.jl",
  [(joinpath("src", "xunittest.jl"), "test"),],
  version = v"1.0"
  )
const pkgdeps = (xunittest, )
####
## End Dependencies
####

## Main run/build function
main = SourceDeps.@mainfn
#   println(macroexpand(:(SourceDeps.@mainfn pkgdeps srcroot pkgroot)))
# main(ARGS, pkgdeps, srcroot, pkgroot)
main(ARGS) # Default call
# main(["clean-all"])
end
