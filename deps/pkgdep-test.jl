module testdeps
using Compat
include(joinpath(dirname(@__FILE__), "..", "src", "SourceDeps.jl"))
using .SourceDeps

SourceDeps.@setup

const reportingjl = PkgDependency(
  "Reporting",
  "git@bitbucket.org:jgoldfar/reporting-git.jl.git",
  version = latestver,
  )
const pkgdeps = (reportingjl, )

const main = SourceDeps.@mainfn
main(["pkg-versions", "install"]) # Default call
# Pkg.installed("Reporting")
# println(readstring(`ls -l /home/jgoldfar/.julia/v0.3`))

# SourceDeps.@clean_all reportingjl
# println(readstring(`ls -l /home/jgoldfar/.julia/v0.3`))
end
