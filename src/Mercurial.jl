function get_repo_clone(repo::AbstractString, dest::AbstractString)
  const clonecmd = `hg clone $repo $dest`
  readstring(clonecmd)
end
function get_repo_update(dest::AbstractString, tag::AbstractString = "")
  const upcmd = `hg --cwd "$dest" update $tag`
  readstring(upcmd)
end
function get_repo_pull(dest::AbstractString)
  const pullcmd = `hg --cwd "$dest" pull`
  readstring(pullcmd)
end
function get_repo_hash(dest::AbstractString)
  const hashcmd = `hg --cwd "$dest" id`
  chomp(readstring(hashcmd))
end