module SourceDeps
using Compat

const ssh_default = true
const get_repo_verbose = false
const verbose = false
const latestver = v"0"
const emptyrepo = ""

export FileDependency, PkgDependency, latestver, emptyrepo, install, clean
export name, hrversion

type FileDependency{T<:AbstractString}
  name::AbstractString
  sshrepo::AbstractString
  @compat installmap::Vector{Tuple{T,T}}
  version::VersionNumber
  httprepo::AbstractString
end

FileDependency{T<:AbstractString}(name::AbstractString,
                          sshrepo::AbstractString,
                          @compat installmap::Vector{Tuple{T,T}};
                          version::VersionNumber = latestver,
                          httprepo::AbstractString = emptyrepo) = FileDependency(name, sshrepo, installmap, version, httprepo)

type PkgDependency
  name::AbstractString
  sshrepo::AbstractString
  version::VersionNumber
end

PkgDependency(name::AbstractString,
              sshrepo::AbstractString;
              version::VersionNumber = latestver) = PkgDependency(name, sshrepo,  version)
@compat typealias Dependency Union{FileDependency, PkgDependency}

macro setup()
  # Assume we are in %project_root%/deps/build.jl
  return esc(quote
               const srcroot = joinpath(dirname(@__FILE__), "src")
               const pkgroot = joinpath(dirname(@__FILE__), "..")
               srcroot, pkgroot
             end #quote
             )
end # macro

name(dep::Dependency) = dep.name
root(dep::FileDependency, srcroot::AbstractString) = joinpath(srcroot, name(dep))
root(dep::PkgDependency, srcroot::AbstractString = "") = Pkg.dir(name(dep))
join(dep::FileDependency, srcroot::AbstractString, paths...) = joinpath(root(dep, srcroot), paths...)
function repo(dep::FileDependency, ssh::Bool = ssh_default)
  if ssh
    return dep.sshrepo
  elseif dep.httprepo == emptyrepo
    error("No http repo.")
    return ""
  else
    return dep.httprepo
  end
end
tag(dep::FileDependency) = ((dep.version==latestver) ? "default" : string("v", dep.version))
tag(dep::PkgDependency) = ((dep.version==latestver) ? "HEAD" : string("v", dep.version))
hrversion(dep::Dependency) = ((dep.version==latestver) ? "latest" : string("v", dep.version))
built(dep::FileDependency, srcroot::AbstractString) = isdir(join(dep, srcroot, ".hg"))
function built(dep::PkgDependency, srcroot::AbstractString)
  try
    flag = isa(Pkg.installed(name(dep)), VersionNumber)
    if (dep.version == latestver) || (Pkg.installed(name(dep))!=dep.version)
      return false
    else
      return true
    end
  catch v
    return false
  end
end

install_files_manifest(dep::FileDependency, srcroot::AbstractString) = join(dep, srcroot, "installed.dat")
function install_files(dep::FileDependency, srcroot::AbstractString, pkgroot::AbstractString)
  println("Installing files for dependency ", name(dep))
  const filemanifest = install_files_manifest(dep, srcroot)
  println("Writing installed file manifest on ", filemanifest)
  open(filemanifest, "w") do st
    for (src, dest) in dep.installmap
      destroot = joinpath(pkgroot, dest); mkpath(destroot)
      src = realpath(join(dep, srcroot, src))
      if isdir(src)
        for srcf in readdir(src)
          destf = joinpath(destroot, basename(srcf))
          srcp = joinpath(src, srcf)
          verbose && println("Copying ", srcp, " to ", destf)
          cp(srcp, destf)
          println(st, destf)
        end
      else
        dest = joinpath(destroot, basename(src))
        verbose && println("Copying ", src, " to ", dest)
        cp(src, dest)
        println(st, dest)
      end
    end #for
  end #do
  return nothing
end
install_files(dep::PkgDependency, srcroot::AbstractString, pkgroot::AbstractString) = warn("Run Pkg.build(\"$(name(dep))\") to finish the installation process")

function uninstall_files(dep::FileDependency, srcroot::AbstractString, pkgroot::AbstractString)
  const filemanifest = install_files_manifest(dep, srcroot)
  println("Uninstalling package ", name(dep), "using manifest ", filemanifest)

  try
    open(filemanifest, "r") do st
      for line in eachline(st)
        chline = chomp(line)
        verbose && println("Removing ", chline)
        rm(chline)
      end
    end #do
  end
  rm(filemanifest)
  return nothing
end
uninstall_files(dep::PkgDependency, srcroot::AbstractString, pkgroot::AbstractString) = 0

function uninstall_dep(dep::FileDependency, srcroot::AbstractString)
  println("Removing package ", name(dep), " source.")
  try
    rm(root(dep, srcroot), recursive = true)
  end
  return nothing
end
function uninstall_dep(dep::PkgDependency, srcroot::AbstractString)
  try
    Pkg.rm(name(dep))
  end
  return nothing
end
function installed_hash(dep::FileDependency, srcroot::AbstractString)
  return get_repo_hash(root(dep, srcroot))
end
function installed_hash(dep::PkgDependency, srcroot::AbstractString = "")
  const work_tree = root(dep, srcroot)
  const git_dir = joinpath(work_tree, ".git")
  const cver = tag(dep)
#   const installed = Pkg.installed(name(dep))
  const cmd = `git --git-dir=$(git_dir) --work-tree=$(work_tree) rev-parse --short $cver`
#   println(cmd)
  try
  const res = chomp(readstring(cmd))
    return res
  catch v
#     println(v)
  end
  return "none"
end

macro mainfn()
  fnname = :main
  esc(quote
        function $(fnname){T<:AbstractString}(cmdlineargs::Vector{T}, pkgdeps = pkgdeps, srcroot::AbstractString = srcroot, pkgroot::AbstractString = pkgroot)
          if in("--SourceDeps-skip-main", cmdlineargs)
            return nothing
          end

          const ssh = SourceDeps.use_ssh(cmdlineargs)
          mkpath(srcroot)

          println(@__FILE__, " called. Arguments: [", join(cmdlineargs, ", "),"]")

          if isempty(cmdlineargs) || in("update-deps", cmdlineargs)
            SourceDeps.install(pkgdeps, srcroot, pkgroot)
            return nothing
          end

          if in("pkg-versions", cmdlineargs)
            for pkgt in pkgdeps
              print("Package: ", name(pkgt), " requires ", hrversion(pkgt), ". Current commit hash: ", SourceDeps.installed_hash(pkgt, srcroot) ,"\n")
            end
          end

          if in("variables", cmdlineargs)
            dump(pkgdeps)
            print("\n")
            dump(cmdlineargs)
            println("\nsrcroot: ", srcroot)
            println("pkgroot: ", pkgroot)
            return nothing
          end

          if (in("usage", cmdlineargs) || in("-h", cmdlineargs) || in("--help", cmdlineargs))
            SourceDeps.usage(pkgdeps, basename(@__FILE__))
            return nothing
          end

          const clean_all = in("clean-all", cmdlineargs)
          if in("clean-deps", cmdlineargs) || clean_all
            SourceDeps.clean(pkgdeps, srcroot, pkgroot, clean_all)
            try
              clean_all && rm(srcroot)
            end
          end

          const checkout = (in("--checkout", cmdlineargs) || in("--latest", cmdlineargs))
          if in("install", cmdlineargs)
            SourceDeps.install(pkgdeps, srcroot, pkgroot, ssh, checkout)
            return nothing
          end

          for pkg in pkgdeps
            if in(string("install-", name(pkg)), cmdlineargs)
              SourceDeps.install(pkg, srcroot, pkgroot,  ssh, checkout)
            end
          end
          return nothing
        end # function
        $fnname
      end
      )
end

# General setup/deps functions
function usage(pkgdeps, basefile)
  usessh = ssh_default ? "ssh" : "https"
  print(basefile, "usage\n\tjulia ", basefile,
        " [--use-ssh] [--use-https] [--help] [-h] [--checkout] [COMMANDS...]\n",
        " --use-ssh forces use of ssh without fallback, while --use-https\n",
        " forces use of https repositories (when available.) By default,\n ",
        usessh, " is used.\n",
        " --checkout forces all dependencies to be installed from the repo latest\n",
        " sources, rather than the dependency-suggested version.\n",
        " -h/--help: Prints this message.\n",
        " COMMANDS is one of\n",
        "\tinstall: Installs all dependencies.\n")
  for pkg in pkgdeps
    @printf "\tinstall-%s: Install %s.\n" name(pkg) name(pkg)
  end
  print("\tclean-deps: Removes deps directories.\n",
        "\tclean-all: Removes deps directories and installed files.\n",
        "\tusage: Prints this message.\n",
        "\n Note that a \"clean\" install can be accomplished by running\n")
  @printf " \"%s clean-all install\". Clean commands are always run before\n" basefile
  print(" installation.\n")
  @printf "\n %s generated by SourceDeps.jl (c) Jonathan Goldfarb 2015\n" basefile
  return nothing
end
function use_ssh{T<:AbstractString}(args::Vector{T})
  const use_https = in("--use-https", args)
  if use_https
    return false
  else
    return in("--use-ssh", args)||!use_https
  end
  return true
end
use_ssh() = ssh_default


function install(pkg::Dependency,
                 srcroot::AbstractString,
                 pkgroot::AbstractString,
                 ssh::Bool = ssh_default,
                 checkout::Bool = false)
  try
    get_repo(pkg, srcroot, checkout)
    install_files(pkg, srcroot, pkgroot)
  catch v
    print(pkg, " gave error ", v)
  end# try/catch
  return nothing
end
function install(pkgdeps, srcroot::AbstractString, pkgroot::AbstractString, ssh::Bool = ssh_default, checkout::Bool = false)
  for pkg in pkgdeps
    install(pkg, srcroot, pkgroot, ssh, checkout)
  end # for
  return nothing
end

function clean(pkg::Dependency, srcroot::AbstractString, pkgroot::AbstractString, all::Bool = false)
  try
    uninstall_files(pkg, srcroot, pkgroot)
    all && uninstall_dep(pkg, srcroot)
  catch v
    if !(isdefined(v, :prefix) && v.prefix == "unlink") # Already deleted
      print(pkg, " gave error ", v)
    end
  end
  return nothing
end
function clean(pkgdeps, srcroot::AbstractString, pkgroot::AbstractString, all::Bool = false)
  for pkg in pkgdeps
    clean(pkg, srcroot, pkgroot, all)
  end # for
  return nothing
end

# Utility functions
include("Mercurial.jl")
function get_repo(repo::AbstractString, dest::AbstractString, tag::AbstractString = "")
  if isdir(dest) && isdir(joinpath(dest, ".hg"))
    try
      println("Already cloned. Pulling repo ", repo, " into ", dest)
      const pres = get_repo_pull(dest)
      get_repo_verbose && println("Pull results: \n", pres)
    catch v
      println("Error during pull: ", v)
    end
  else
    try
      println("Cloning ", repo, " into ", dest)
      const cres = get_repo_clone(repo, dest)
      get_repo_verbose && print("Clone results: \n", cres, "\n")
    catch v
      println("Error during clone: ", v)
    end
  end
  try
    const tagto = (tag == "") ? "latest" : tag
    println("Updating to ", tagto, ".")
    const ures = get_repo_update(dest, tag)
    get_repo_verbose && print("Update results: \n", ures, "\n")
  catch v
    println("Either the repo ", repo, " or tag ", tag, "does not exist.\n",
    "Error during update: ", v, ". Try again!")
  end
  return nothing
end
function get_repo(sshrepo::AbstractString, httprepo::AbstractString, dest::AbstractString, tag::AbstractString)
  try
    get_repo(sshrepo, dest, tag)
    return nothing
  catch v
    @printf "Retrieving SSH repo %s gave error %s.\n Trying %s" sshrepo v httprepo
  end
  try
    get_repo(httprepo, dest, tag)
    return nothing
  catch v
    @printf "Retrieving HTTP repo %s gave error %s.\n Try again!" httprepo v2
  end
  return -1
end
function get_repo(dep::FileDependency, srcroot::AbstractString, checkout::Bool = false, ssh::Bool = ssh_default)
  if checkout
    get_repo(repo(dep, ssh), root(dep, srcroot), "")
  else
    get_repo(repo(dep, ssh), root(dep, srcroot), tag(dep))
  end
  return nothing
end
function get_repo(dep::PkgDependency, srcroot::AbstractString, checkout::Bool = false)
  try
    Pkg.clone(dep.sshrepo, name(dep))
  end
  if checkout || (!checkout && (dep.version == latestver))
    Pkg.checkout(name(dep))
  else
    Pkg.checkout(name(dep), dep.version)
  end
  return nothing
end #function

end # module
